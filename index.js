var locations = {
    moscow: [55.746215, 37.622504],
    spb: [59.939095, 30.315868],
    ufa: [54.735147, 55.958727],
    nn: [56.326797, 44.006516],
    voronezh: [51.660781, 39.200269],
    grozniy: [43.317881, 45.695358],
    ekb: [56.838011, 60.597465],
    novosib: [55.030199, 82.920430],
    krasnodar: [45.035470, 38.975313],
    krasnogorsk: [55.831099, 37.330192],
    rnd: [47.222078, 39.720349],
    mytishchi: [55.910483, 37.736402],
    balashiha: [55.796339, 37.938199],
    stavropol: [45.044521, 41.969083],
    kaliningrad: [54.710454, 20.512733],
    penza: [53.195063, 45.018316],
    sochi: [43.585525, 39.723062],
    yakutsk: [62.027757, 129.731235],
    tyumen: [57.153033, 65.534328],
    chelyabinsk: [55.159897, 61.402554],
    vladikavkaz: [43.021150, 44.681960],
    surgut: [61.254035, 73.396221],
    ulyanovsk: [54.314192, 48.403123],
    nizhnevartovsk: [60.938545, 76.558902],
    gelendzhik: [44.561141, 38.076809],
    blagoveschensk: [50.290640, 127.527173]
};

window.locations = locations;

$(document).ready(function() {
    if (!window.city) {
        window.city = "moscow"
    }

    $('input[type=file]').change(function(e) {
        $in = $(this);
        $in.next().html($in.val());

    });

    $('.uploadButton').click(function() {
        var fileName = $("#fileUpload").val();
        if (fileName) {
            alert(fileName + " can be uploaded.");
        } else {
            alert("Please select a file to upload");
        }
    });


    $('#qr-code').qrcode({
        text: "http://www.google.com",
        width: 80,
        height: 80,
        background: "#ffffff",
        foreground: "#000000"
    });

    $(".list-of-cities li").click(function(e) {
        e.preventDefault();
        $(".list-of-cities li").removeClass('city-active');
        $(this).addClass('city-active');

        // $(".dropdown .btn").html($(this).text());

        // $(".dropdown .btn").append('<span><img src="/img/arrow-down.svg" height="9px" width="16px /"></span>')


        var selectedLocation = $(this).data('location');

        map.setCenter(locations[selectedLocation]);
        map.setZoom(12);
        window.city = selectedLocation
    })

    $('#dropdown').change(function() {
        var val = $("#dropdown option:selected").attr('data-location');
        map.setCenter(locations[val]);
        map.setZoom(12);
        window.city = val;
    });

    $(".btn-reg").click(function() {
        $("#myModal").modal('show');
        $("#registration").removeClass("no-active");
        $("#forgot-pswd").addClass("no-active");
        $("#restore-pswd").addClass("no-active");
        $("#login").addClass("no-active");
        $("#edit-profile").addClass("no-active");
        $(".confirmation").addClass("no-active");
        $(".registration a[href^='#registration']").addClass("isActive");
    });

    $(".edit-btn").click(function() {
        $("#myModal").modal('show');
        $("#edit-profile").removeClass("no-active");
        $("#registration").addClass("no-active");
        $("#forgot-pswd").addClass("no-active");
        $("#restore-pswd").addClass("no-active");
        $("#login").addClass("no-active");
    });


    $(".registration a[href^='#login']").click(function() {
        $("#registration").addClass("no-active");
        $(".registration a[href^='#registration']").removeClass("isActive");
        $("#login").removeClass("no-active");
        $(".login a[href^='#login']").addClass("isActive");
    });

    $(".login a[href^='#registration']").click(function() {
        $("#login").addClass("no-active");
        $(".login a[href^='#login']").removeClass("isActive");
        $("#registration").removeClass("no-active");
        $(".registration a[href^='#registration']").addClass("isActive");
    });

    $("a[href^='#forgot-pswd']").click(function() {
        $("#forgot-pswd").removeClass("no-active");
        $("#registration").addClass("no-active");
        $("#login").addClass("no-active");
        $(".registration a[href^='#registration']").removeClass("isActive");
        $(".login a[href^='#login']").removeClass("isActive");
    });

    $("#forgot-pswd .modal-footer .btn-primary").click(function() {
        $("#forgot-pswd").addClass("no-active");
        $("#restore-pswd").removeClass("no-active");
    });

    $(".back").click(function() {
        $("#forgot-pswd").addClass("no-active");
        $("#login").removeClass("no-active");
        $(".login a[href^='#login']").addClass("isActive");
    });

    $("#restore-pswd .modal-footer .btn-primary").click(function() {
        $("#restore-pswd").addClass("no-active");
        $("#myModal").modal('hide');
    });

    $("").click(function() {
        $("#restore-pswd").addClass("no-active");
        $("#myModal").modal('hide');
    });

    $('.menu__burger').on('click', function(e) {
        e.preventDefault;
        $(this).toggleClass('menu__burger--active');
        $('.menu-popup').toggleClass('active');
    })

    $(window).scroll(hideBlock);

    function hideBlock() {
        $('.menu-popup').removeClass('active');
        $('.menu__burger').removeClass('menu__burger--active')
    }


    $('#checkbox').on('change', function() {
        if ($('#checkbox').prop('checked')) {
            $('#submit').attr('disabled', false);
        } else {
            $('#submit').attr('disabled', true);
        }
    });


    $('#checkbox-1').on('change', function() {
        if ($('#checkbox-1').prop('checked')) {
            $('#submit-1').attr('disabled', false);
        } else {
            $('#submit-1').attr('disabled', true);
        }
    });

    let inputsArrId = ['nameInput', 'emailInput', 'themeMessageSelect', 'messageInput']
    let inputs = {
        name: false,
        email: false,
        themMessage: false,
        message: false
    }

    let inputsArrId1 = ['surnameInput-1', 'nameInput-1', 'middleNameInput-1', 'emailInput-1', 'pswdInput-1', 'rePswdInput-1']
    let inputs1 = {
        surname: false,
        name: false,
        middleName: false,
        email: false,
        pswd: false,
        rePswd: false
    }

    let inputsArrId2 = ['emailInput-2', 'pswdInput-2']
    let inputs2 = {
        email: false,
        pswd: false
    }

    let inputsArrId3 = ['emailInput-3']
    let inputs3 = {
        email: false
    }

    let inputsArrId4 = ['pswdInput-3', 'rePswdInput-2']
    let inputs4 = {
        pswd: false,
        rePswd: false
    }

    $('#nameInput').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs.name === false) {
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
            inputs.name = true
        }
        if ($(this).val().length === 0 && inputs.name === true) {
            inputs.name = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#emailInput').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.email === false) {
            inputs.email = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
        }
        if ($(this).val().length === 0 && inputs.email === true) {
            inputs.email = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#themeMessageSelect').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.themMessage === false) {
            inputs.themMessage = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
        }
        if ($(this).val().length === 0 && inputs.themMessage === true) {
            inputs.themMessage = false
            inputsArrId.push($(this).attr('id'))
        }
    })

    $('#messageInput').on('change keyup paste', function() {
        $(this).removeClass('error')

        if ($(this).val().length > 0 && inputs.message === false) {
            inputs.message = true
            for (let i = 0; i <= inputsArrId.length; i++) {
                if ($(this).attr('id') === inputsArrId[i]) {
                    inputsArrId.splice(i, 1)
                }
            }
        }
        if ($(this).val().length === 0 && inputs.message === true) {
            inputs.message = false
            inputsArrId.push($(this).attr('id'))
        }
    })


    $('#surnameInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.surname === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.surname = true
        }
        if ($(this).val().length === 0 && inputs1.surname === true) {
            inputs1.surname = false
            inputsArrId1.push($(this).attr('id'))
        }
    })

    $('#nameInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.name === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.name = true
        }
        if ($(this).val().length === 0 && inputs1.name === true) {
            inputs1.name = false
            inputsArrId1.push($(this).attr('id'))
        }
    })


    $('#middleNameInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.middleName === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.middleName = true
        }
        if ($(this).val().length === 0 && inputs1.middleName === true) {
            inputs1.middleName = false
            inputsArrId1.push($(this).attr('id'))
        }
    })


    $('#emailInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.email === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.email = true
        }
        if ($(this).val().length === 0 && inputs1.email === true) {
            inputs1.email = false
            inputsArrId1.push($(this).attr('id'))
        }
    })


    $('#pswdInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.pswd === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.pswd = true
        }
        if ($(this).val().length === 0 && inputs1.pswd === true) {
            inputs1.pswd = false
            inputsArrId1.push($(this).attr('id'))
        }
    })


    $('#rePswdInput-1').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs1.rePswd === false) {
            for (let i = 0; i <= inputsArrId1.length; i++) {
                if ($(this).attr('id') === inputsArrId1[i]) {
                    inputsArrId1.splice(i, 1)
                }
            }
            inputs1.rePswd = true
        }
        if ($(this).val().length === 0 && inputs1.rePswd === true) {
            inputs1.rePswd = false
            inputsArrId1.push($(this).attr('id'))
        }
    })

    $('#emailInput-2').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs2.email === false) {
            for (let i = 0; i <= inputsArrId2.length; i++) {
                if ($(this).attr('id') === inputsArrId2[i]) {
                    inputsArrId2.splice(i, 1)
                }
            }
            inputs2.email = true
        }
        if ($(this).val().length === 0 && inputs2.email === true) {
            inputs2.email = false
            inputsArrId2.push($(this).attr('id'))
        }
    })


    $('#pswdInput-2').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs2.pswd === false) {
            for (let i = 0; i <= inputsArrId2.length; i++) {
                if ($(this).attr('id') === inputsArrId2[i]) {
                    inputsArrId2.splice(i, 1)
                }
            }
            inputs2.pswd = true
        }
        if ($(this).val().length === 0 && inputs2.pswd === true) {
            inputs2.pswd = false
            inputsArrId2.push($(this).attr('id'))
        }
    })

    $('#emailInput-3').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs3.email === false) {
            for (let i = 0; i <= inputsArrId3.length; i++) {
                if ($(this).attr('id') === inputsArrId3[i]) {
                    inputsArrId3.splice(i, 1)
                }
            }
            inputs3.email = true
        }
        if ($(this).val().length === 0 && inputs3.email === true) {
            inputs3.email = false
            inputsArrId3.push($(this).attr('id'))
        }
    })

    $('#pswdInput-3').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs4.pswd === false) {
            for (let i = 0; i <= inputsArrId4.length; i++) {
                if ($(this).attr('id') === inputsArrId4[i]) {
                    inputsArrId4.splice(i, 1)
                }
            }
            inputs4.pswd = true
        }
        if ($(this).val().length === 0 && inputs4.pswd === true) {
            inputs4.pswd = false
            inputsArrId4.push($(this).attr('id'))
        }
    })

    $('#rePswdInput-2').on('change keyup paste', function() {
        $(this).removeClass('error')
        if ($(this).val().length > 0 && inputs2.rePswd === false) {
            for (let i = 0; i <= inputsArrId4.length; i++) {
                if ($(this).attr('id') === inputsArrId4[i]) {
                    inputsArrId4.splice(i, 1)
                }
            }
            inputs4.rePswd = true
        }
        if ($(this).val().length === 0 && inputs4.rePswd === true) {
            inputs4.rePswd = false
            inputsArrId4.push($(this).attr('id'))
        }
    })


    $('#submit').on('click', function(e) {
        for (let i = 0; i <= inputsArrId.length; i++) {
            $('#' + inputsArrId[i] + '').addClass('error')
        }
        if (inputsArrId.length == 0) {
            $('#feedback form').addClass('no-active');
            $('#feedback .success').removeClass('no-active');
            $('#nameInput, #emailInput, #themeMessageSelect, #messageInput').val('')
            inputsArrId = ['nameInput', 'emailInput', 'themeMessageSelect', 'messageInput']
            inputs = {
                name: false,
                email: false,
                themMessage: false,
                message: false
            }
        }
    })


    $('#submit-1').on('click', function(e) {
        for (let i = 0; i <= inputsArrId1.length; i++) {
            $('#' + inputsArrId1[i] + '').addClass('error')
        }
        if (inputsArrId1.length == 0) {
            $('#registration').addClass('no-active');
            $('#myModal .confirmation').removeClass('no-active');
            $('#surnameInput-1, #nameInput-1, #middleNameInput-1, #emailInput-1, #pswdInput-1, #rePswdInput-1').val('')
            inputsArrId1 = ['surnameInput-1', 'nameInput-1', 'middleNameInput-1', 'emailInput-1', 'pswdInput-1', 'rePswdInput-1']
            inputs1 = {
                surname: false,
                name: false,
                middleName: false,
                email: false,
                pswd: false,
                rePswd: false
            }
        }
    })

    $('#submit-2').on('click', function(e) {
        for (let i = 0; i <= inputsArrId2.length; i++) {
            $('#' + inputsArrId2[i] + '').addClass('error')
        }
        if (inputsArrId2.length == 0) {
            $('#emailInput-2, #pswdInput-2').val('')
            inputsArrId2 = ['emailInput-2', 'pswdInput-2']
            inputs2 = {
                email: false,
                pswd: false
            }
        }
    })


    $('#submit-3').on('click', function(e) {
        for (let i = 0; i <= inputsArrId3.length; i++) {
            $('#' + inputsArrId3[i] + '').addClass('error')
        }
        if (inputsArrId3.length == 0) {
            $('#emailInput-3').val('')
            inputsArrId3 = ['emailInput-3']
            inputs3 = {
                email: false
            }
        }
    })

    $('#submit-4').on('click', function(e) {
        for (let i = 0; i <= inputsArrId4.length; i++) {
            $('#' + inputsArrId4[i] + '').addClass('error')
        }
        if (inputsArrId4.length == 0) {
            $('#pswdInput-3, #rePswdInput-2').val('')
            inputsArrId1 = ['pswdInput-3', 'rePswdInput-2']
            inputs4 = {
                pswd: false,
                rePswd: false
            }
        }
    })

    document.addEventListener('invalid', (function() {
        return function(e) {
            e.preventDefault();
            // document.getElementById("nameInput").focus();
            // document.getElementById("emailInput").focus();
            // document.getElementById("themeMessageSelect").focus();
            // document.getElementById("messageInput").focus();
        };
    })(), true);
})